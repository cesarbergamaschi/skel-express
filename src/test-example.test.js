import test from 'ava';

test(`
  [test example]
  
  An test example
`, (t) => {
  const str = 'test';
  const expected = 'test';

  t.is(str, expected);
});
