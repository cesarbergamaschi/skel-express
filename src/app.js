import express from 'express';

import initApp from './bootstrap';

const app = express();

(() => initApp(app, express))();
