import dotenv from 'dotenv';

dotenv.load();

/**
 * Social Login Configuration
 */

const config = {
  facebookAuth: {},
  twitterAuth: {},
  googleAuth: {},
};

/**
 * Facebook config
 */

config.facebookAuth.clientID = '';
config.facebookAuth.clientSecret = '';
config.facebookAuth.callbackURL = '';

/**
 * Twitter config
 */

config.twitterAuth.consumerKey = '';
config.twitterAuth.consumerSecret = '';
config.twitterAuth.callbackURL = '';

/**
 * Google config
 */

config.googleAuth.clientID = '';
config.googleAuth.clientSecret = '';
config.googleAuth.callbackURL = '';

export default config;
