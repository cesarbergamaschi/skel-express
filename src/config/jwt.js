import dotenv from 'dotenv';

dotenv.config();

/**
 * JWY Configuration
 */

const config = {};

config.secret = 'secretExample';

export default config;
