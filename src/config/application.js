import dotenv from 'dotenv';

dotenv.config();

/**
 * Basic Configuration
 */

const config = {};

config.port = parseInt(process.env.SERVER_PORT, 10) || 3000;

config.useApiKey = process.env.USE_API_KEY || true;
config.apiKey = process.env.API_KEY || 'key-example';

config.useCors = true;
config.useUrlEncoded = true;
config.useExpressStatic = true;
config.useJsonParser = true;
config.useMorganCombined = true;

export default config;
