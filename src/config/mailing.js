import dotenv from 'dotenv';

dotenv.load();

/**
 * Mailing Configuration
 */

export const config = {};

config.smtp = {};
config.smtp.name = process.env.SMTP_FROM_NAME || 'support';
config.smtp.address = process.env.SMTP_FROM_ADDRESS || 'smtp.mailtrap.io';
config.smtp.user = process.env.SMTP_USER || 'username';
config.smtp.pass = process.env.SMTP_PASS || 'password';
config.smtp.port = process.env.SMTP_PORT || '2525';
config.smtp.secure = process.env.SMTP_SECURE === 'true' || false;
