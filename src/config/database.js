import dotenv from 'dotenv';

dotenv.config();

/**
 * Database Configuration
 */

const config = {};

config.mongoDb = {};
config.mongoDb.useDatabase = true;
config.mongoDb.mongooseConnection = {};
config.mongoDb.connectionProperties = {};

config.mongoDb.mongooseConnection.useCreateIndex = true;
config.mongoDb.mongooseConnection.useNewUrlParser = true;
config.mongoDb.mongooseConnection.useUnifiedTopology = true;

config.mongoDb.connectionProperties.url =
  process.env.MONGODB_URL || 'localhost';
config.mongoDb.connectionProperties.database =
  process.env.MONGODB_DATABASE || 'database';
config.mongoDb.connectionProperties.user = process.env.MONGODB_USER || 'root';
config.mongoDb.connectionProperties.pass = process.env.MONGODB_PASS || '';
config.mongoDb.connectionProperties.authSource =
  process.env.MONGODB_AUTHSOURCE || '';

export default config;
