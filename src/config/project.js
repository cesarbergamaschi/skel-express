import dotenv from 'dotenv';

import * as pkg from '../../package.json';

dotenv.config();

/**
 * App Configuration
 */

const config = {};

// From package.json
config.name = pkg.name;
config.version = pkg.version;
config.description = pkg.description;
config.company = pkg.company;
config.author = pkg.author;
config.keywords = pkg.keywords;

export default config;
