import { logger } from 'core';

import projectConfig from '../../config/project';
import applicationConfig from '../../config/application';

export default async (app) => {
  await app.listen(applicationConfig.port, () => {
    logger.info(
      `${projectConfig.name} app listening on port ${applicationConfig.port}!`
    );
  });
};
