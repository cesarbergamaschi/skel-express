import bootstrapRoutes from '../../app/routes';
import bootstrapServer from './bootstrap-server';

export default async (app, express) => {
  bootstrapRoutes(app, express);

  await bootstrapServer(app);
};
