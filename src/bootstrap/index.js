import __INIT__ from 'core';

import bootstrapApp from './bootstrap-app';
import databaseConfig from '../config/database';
import applicationConfig from '../config/application';
import bootstrapDatabase from './bootstrap-database/mongo/connect';

const config = {
  databaseConfig,
  applicationConfig,
};

export default async function initBootstrap(app, express) {
  await __INIT__({ app, config });

  await bootstrapDatabase(config);
  await bootstrapApp(app, express);
}
