import { logger } from 'core';
import mongoose from 'mongoose';

import uri from './create-uri';
import config from '../../../config/database';

const connectionSuccess = () => {
  logger.info('MongoDB connected!');
};

const connectionError = () => {
  logger.info('MongoDB Connection Error. Please make sure MongoDB is running.');
  process.exit(0);
};

export default async () => {
  if (config.mongoDb.useDatabase) {
    await mongoose
      .connect(uri, config.mongoDb.mongooseConnection)
      .then(connectionSuccess, connectionError)
      .catch(connectionError);
  }
};
