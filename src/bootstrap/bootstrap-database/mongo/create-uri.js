import config from '../../../config/database';

// TODO Refactor urgently
// eslint-disable-next-line import/no-mutable-exports
let uri = 'mongodb://';

if (
  config.mongoDb.connectionProperties.user !== '' &&
  config.mongoDb.connectionProperties.pass !== ''
) {
  uri += `${config.mongoDb.connectionProperties.user}:${config.mongoDb.connectionProperties.pass}@`;
}

uri += `${config.mongoDb.connectionProperties.url}/${config.mongoDb.connectionProperties.database}?`;

if (config.mongoDb.connectionProperties.authSource !== '') {
  uri += `authSource=${config.mongoDb.connectionProperties.authSource}`;
}

export default uri;
