import { encrypt, checkEncryption } from '../encriptation';

const checkPassword = async ({ data, password }) => {
  if (data) {
    const { password: encryptedPassword } = data;

    return checkEncryption(password, encryptedPassword);
  }

  return false;
};

const encryptPassword = async (args) => {
  const { password: rawPassword } = args;

  const password = await encrypt(rawPassword);

  return Object.assign(args, {
    password,
  });
};

export { checkPassword, encryptPassword };
