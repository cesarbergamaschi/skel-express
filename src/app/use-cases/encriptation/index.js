import bcrypt from 'bcrypt';

const saltRounds = 10;

const encrypt = async (str) => bcrypt.hash(str, saltRounds);

const checkEncryption = async (str, encryptedStr) =>
  bcrypt.compare(str, encryptedStr);

export { encrypt, checkEncryption };
