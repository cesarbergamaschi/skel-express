import jwt from 'jsonwebtoken';

import config from '../../../config/jwt';

export function signJwt(userId) {
  const { secret } = config;

  return jwt.sign({ userId }, secret, { expiresIn: '30d' });
}
