import jwt from 'jsonwebtoken';

import config from '../../../config/jwt';

export function getHeaderAuthorizationBearer(token) {
  if (token) {
    const authorization = token.split(' ');

    if (authorization[1]) {
      return authorization[1];
    }
  }

  return false;
}

export function getJWTDecodedObject(authorization) {
  try {
    const header = getHeaderAuthorizationBearer(authorization);

    if (header) {
      return jwt.verify(header, config.secret);
    }

    return { message: 'Incomplete header' };
  } catch (e) {
    return e;
  }
}
