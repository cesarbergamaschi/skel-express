import Model from '../../models/example/user-model';

Model.findByEmail = (email) => Model.findOne({ email });

export default Model;
