import Model from '../../models/example/user-model';
import { MONGO_LIMIT_PAGINATE } from '../../constants/mongo-constants';

Model.get = () => Model.find({}).exec();

Model.show = (_id) => Model.findOne({ _id });

Model.addUser = (args) => new Model(args).save();

Model.updateUser = (userId, { name, email, password, bio, birthdate }) =>
  Model.findByIdAndUpdate(
    userId,
    {
      name,
      email,
      password,
      bio,
      birthdate,
    },
    { new: true }
  );

Model.softDelete = (_id, deletedBy) => Model.delete({ _id }, deletedBy);

Model.restoreDelete = (_id) => Model.restore({ _id });

Model.paginated = (page) =>
  Model.paginate({}, { page, limit: MONGO_LIMIT_PAGINATE });

export default Model;
