import { transformerCore } from 'core';

import { getJWTDecodedObject } from '../use-cases/jwt/jwt-decoder';

export default async (req, res, next) => {
  const decoded = getJWTDecodedObject(req.headers.authorization);

  return decoded && decoded.userId
    ? next()
    : res.status(403).send(
        transformerCore.errorTransformer({
          name: 'Invalid',
          message: decoded.message || 'Unknown exception',
        })
      );
};
