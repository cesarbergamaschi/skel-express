import { transformerCore, serviceCore } from 'core';

import repository from '../../repositories/example/user-repository';
import transformer from '../../transformers/example/user-transformer';
import { encryptPassword } from '../../use-cases/user/password-encrypter';

const { sendResponse } = serviceCore;

export async function getUsers(req) {
  const data = await repository.get();
  const transform = transformer(data);

  return sendResponse(req, { data, transform });
}

export async function getUser(req, userId) {
  const data = await repository.show(userId);
  const transform = transformer(data, transformerCore.SINGLE_RESULT_TRANSFORM);

  return sendResponse(req, { data, transform });
}

export async function addUser(req, args) {
  await encryptPassword(args);

  const data = await repository.addUser(args);
  const transform = transformer(data, transformerCore.SINGLE_RESULT_TRANSFORM);

  return sendResponse(req, { data, transform });
}

export async function updateUser(req, userId, args) {
  const data = await repository.updateUser(userId, args);
  const transform = transformer(data, transformerCore.SINGLE_RESULT_TRANSFORM);

  return sendResponse(req, { data, transform });
}

export async function deleteUser(req, userId) {
  const data = await repository.softDelete(userId);

  return sendResponse(req, { data });
}

export async function restoreUser(req, userId) {
  const data = await repository.restoreDelete(userId);

  return sendResponse(req, { data });
}

/* export async function paginated(page) {
  const data = await repository.paginated(page);

  return sendResponse(200, transformer(data));
} */
