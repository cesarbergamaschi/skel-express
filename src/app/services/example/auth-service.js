import { transformerCore, serviceCore } from 'core';

import transformer from '../../transformers/example/auth-transformer';
import authRepository from '../../repositories/example/auth-repository';
import userRepository from '../../repositories/example/user-repository';
import { getJWTDecodedObject } from '../../use-cases/jwt/jwt-decoder';
import { checkPassword } from '../../use-cases/user/password-encrypter';

const { sendResponse } = serviceCore;

export async function auth(req, args) {
  const { email, password } = args;
  const data = await authRepository.findByEmail(email);
  const check = await checkPassword({ data, password });

  if (check) {
    const transform = transformer(
      data,
      transformerCore.SINGLE_RESULT_TRANSFORM
    );

    return sendResponse(req, { defaultStatus: 200, data, transform });
  }

  return sendResponse(req, { forceStatus: 403, data: null });
}

export async function authenticatedRoute(req, authorization) {
  const { userId } = getJWTDecodedObject(authorization);
  const data = await userRepository.show(userId);
  const transform = transformer(data, transformerCore.SINGLE_RESULT_TRANSFORM);

  return sendResponse(req, { data, transform });
}
