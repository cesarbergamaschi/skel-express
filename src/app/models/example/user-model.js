import mongoose from 'mongoose';
import mongooseDelete from 'mongoose-delete';
import mongoosePaginate from 'mongoose-paginate';

const schema = mongoose.Schema(
  {
    name: {
      type: String,
      require: true,
    },
    email: {
      type: String,
      required: false,
      index: {
        unique: true,
      },
    },
    password: {
      type: String,
      require: true,
    },
    bio: {
      type: String,
    },
    birthdate: {
      type: Date,
      require: true,
    },
  },
  {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt',
    },
  }
);

schema.plugin(mongoosePaginate);
schema.plugin(mongooseDelete, {
  overrideMethods: ['count', 'find', 'findOne', 'findByIdAndUpdate', 'update'],
  deletedBy: true,
  deletedAt: true,
});

export default mongoose.model('User', schema);
