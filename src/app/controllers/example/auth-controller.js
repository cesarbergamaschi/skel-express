import { validatorCore } from 'core';

import * as service from '../../services/example/auth-service';
import { authSchema } from '../../validators/example/auth-validator';

const { validateSchema } = validatorCore;

export async function auth(req, res) {
  const validate = await validateSchema(req.body, authSchema, res);

  if (validate) {
    const response = await service.auth(req, req.body);

    res.status(response.status).send(response.data);
  }
}

export async function authenticatedRoute(req, res) {
  const { authorization } = req.headers;

  const response = await service.authenticatedRoute(req, authorization);

  res.status(response.status).send(response.data);
}
