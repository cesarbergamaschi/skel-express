import { validatorCore } from 'core';

import * as service from '../../services/example/user-service';
import { userSchema } from '../../validators/example/user-validator';

const { validateSchema } = validatorCore;

export async function getUsers(req, res) {
  const response = await service.getUsers(req);

  res.status(response.status).send(response.data);
}

export async function getUser(req, res) {
  const response = await service.getUser(req, req.params.userId);

  res.status(response.status).send(response.data);
}

export async function addUser(req, res) {
  const validate = await validateSchema(req.body, userSchema, res);

  if (validate) {
    const response = await service.addUser(req, req.body);

    res.status(response.status).send(response.data);
  }
}

export async function updateUser(req, res) {
  const validate = await validateSchema(req.body, userSchema, res);

  if (validate) {
    const response = await service.updateUser(req, req.params.userId, req.body);

    res.status(response.status).send(response.data);
  }
}

export async function deleteUser(req, res) {
  const response = await service.deleteUser(req, req.params.userId);

  res.status(response.status).send(response.data);
}

export async function restoreUser(req, res) {
  const response = await service.restoreUser(req, req.params.userId);

  res.status(response.status).send(response.data);
}

/* export async function paginated(req, res) {
  /!* const response = await service.paginated(req.params.page);
 
 res.status(response.status).send(response.data); *!/
} */
