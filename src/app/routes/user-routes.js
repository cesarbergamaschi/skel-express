import * as controller from '../controllers/example/user-controller';

module.exports = (app) => {
  app.get('/example/users', [], (req, res) => {
    controller.getUsers(req, res);
  });

  app.get('/example/users/:userId', [], (req, res) => {
    controller.getUser(req, res);
  });

  app.post('/example/users', [], (req, res) => {
    controller.addUser(req, res);
  });

  app.put('/example/users/:userId', [], (req, res) => {
    controller.updateUser(req, res);
  });

  app.delete('/example/users/:userId', [], (req, res) => {
    controller.deleteUser(req, res);
  });

  app.put('/example/users/:userId/restore', [], (req, res) => {
    controller.restoreUser(req, res);
  });

  /* app.get('/users/page=:page', [], (req, res) => {
   controller.paginated(req, res);
   }); */
};
