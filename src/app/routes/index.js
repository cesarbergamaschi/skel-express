import { routeCore } from 'core';

import { paramCase } from 'change-case';
import requireDir from 'require-dir';

export default (app, express) => {
  const routes = requireDir();
  const router = express.Router();

  // Initialize all routes
  Object.keys(routes).forEach((routeName) => {
    // You can add some middleware here
    // router.use(someMiddleware);

    // Initialize the route to add its functionality to router
    // eslint-disable-next-line import/no-dynamic-require
    require(`./${routeName}`)(app);

    // Add router to the speficied route name in the app
    app.use(`/${paramCase(routeName)}`, router);
  });

  routeCore.notFound(app);
};
