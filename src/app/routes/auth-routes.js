import * as controller from '../controllers/example/auth-controller';
import bearer from '../middlewares/authorization';

module.exports = (app) => {
  app.post('/example/auth', [], (req, res) => {
    controller.auth(req, res);
  });

  app.get('/example/authenticated-route', [bearer], (req, res) => {
    controller.authenticatedRoute(req, res);
  });
};
