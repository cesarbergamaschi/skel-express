import repository from '../../repositories/example/auth-repository';

function emailExist(yup) {
  yup.addMethod(yup.string, 'existEmail', function addMethod() {
    return this.test(
      'emailExistTest',
      'Test if e-mail exists in database',
      async function testMethod(value) {
        const { path, createError } = this;
        const error = createError({
          path,
          message: `${path} already exists`,
        });

        const data = await repository.findByEmail(value);

        if (data) {
          return error;
        }

        return true;
      }
    );
  });
}

export function addExistEmailMethod(yup) {
  emailExist(yup);
}
