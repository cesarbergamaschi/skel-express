import { validatorCore } from 'core';

const { yup } = validatorCore;

export const authSchema = yup.object().shape({
  email: yup.string().required().max(100),
  password: yup.string().required().min(6),
});
