import { validatorCore } from 'core';

import { addExistEmailMethod } from '../custom-validators/exist-email-validator';

const { yup } = validatorCore;

addExistEmailMethod(yup);

export const userSchema = yup.object().shape({
  name: yup.string().required().min(2).max(100),
  email: yup.string().existEmail().required().max(100),
  password: yup.string().required().min(6),
  bio: yup.string().max(1000),
  birthdate: yup.date().required(),
});
