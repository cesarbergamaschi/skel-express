import { transformerCore } from 'core';

const { Transformer } = transformerCore;

const objectTransformer = {
  id: '_id',
  name: 'name',
  email: 'email',
  bio: 'bio',
  birthdate: 'birthdate',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
};

export default (data, options = {}) => {
  const map = {
    list: 'data',
    item: objectTransformer,
    operate: [],
  };

  const transformer = new Transformer(data, map, options);

  return transformer.transform();
};
