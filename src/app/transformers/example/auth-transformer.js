import { transformerCore } from 'core';

import { signJwt } from '../../use-cases/jwt/sign-jwt';

const { Transformer } = transformerCore;

const objectTransformer = {
  id: '_id',
  name: 'name',
  email: 'email',
  bio: 'bio',
  birthdate: 'birthdate',
};

export default (data, options = {}) => {
  const map = {
    list: 'data',
    item: objectTransformer,
    operate: [
      {
        run: () => signJwt(data._id),
        on: 'jwt',
      },
    ],
  };

  const transformer = new Transformer(data, map, options);

  return transformer.transform();
};
