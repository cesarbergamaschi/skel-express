import mongoose from 'mongoose';

const { Types } = mongoose;

export function isObjectIdValid(value) {
  return Types.ObjectId.isValid(value);
}
